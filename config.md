# Fonctionnalité 9 : Permettre la configuration du jeu via l'interface graphique


Dans cette fonctionnalité, nous allons ajouter un certain nombre de widgets à notre fenêtre principale (la fenêtre blanche laissée vide) pour permettre :

+ De choisir la taille de l'univers.
+ De choisir le pattern
+ Pour placer le pattern
+ ...


Si vous avez fini, vous pouvez maintenant ajouter d'autres fonctionnalités de votre choix à votre jeu. 
 
## Pour finir 

+ <span style='color:blue'>Faire un commit de vos derniers changements.</span> 
+ <span style='color:blue'>Tagger ce dernier commit </span> 
+ <span style='color:blue'>Pousser (Push) votre code vers votre dépôt distant sur GitLab.</span> 
+ <span style='color:blue'>Faire un test de couverture de code de votre MVP et pousser le bilan obtenu vers votre dépôt distant sur GitLab.</span>