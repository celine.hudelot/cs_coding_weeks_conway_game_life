# Fonctionnalité 9 : Gestion des paramètres avec `argparse`

L'objectif ici est de permettre à un utilisateur donné de pouvoir lancer le programme de simulation en ligne de commande en spécifiant les différents paramètres de la simulation en ligne de commande.

On utilisera pour cela le module `argparse` de python dont une documentation est disponible [ici](https://docs.python.org/fr/3/howto/argparse.html).

Formez vous rapidement à ce module et utilisez le pour réaliser la fonctionnalité.

Nous arrivons bientôt à la fin de notre MVP. Il faut juste finir tout cela avec la  [**Fonctionnalité 10** : On met tout cela dans un programme principal](./S4_gamemain.md)
 




