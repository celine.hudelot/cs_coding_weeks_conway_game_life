# Fonctionnalité 10 : On met tout cela dans un programme principal

Il s'agit maintenant de mettre tout ce que nous avons fait dans un programme principal



Pour cela il faut donc ajouter une fonction principale de la manière suivante

```PYTHON
if __name__ == '__main__':
...

```
Un peu de documentation [ici](http://interactivepython.org/runestone/static/CS152f17/Functions/mainfunction.html) et [là](https://www.guru99.com/learn-python-main-function-with-examples-understand-main.html).


A ce stade, nous avons terminé notre objectif 1 et nous avons donc notre MVP pour le jeu de la vie.

Avant de passer à la suite et de mettre à jour votre dépôt git, nous vous demandons de bien prendre le temps d'ajouter un maximum de tests unitaires à votre projet (si vous avez appliqué la méthodologie TDD, cela ne devrait pas vous prendre trop de temps) et de commenter vos différentes fonctions. 


## Retours utilisateurs sur votre jeu

A ce stade, nous allons aussi essayer d'avoir des retours utilisateurs sur ce MVP. Vous allez donc prendre ce role et et tester votre jeu en *jouant* avec. 

Quels sont vos retours utilisateurs sur ce MVP ? Y-a-t'il des fonctionnalités manquantes et que vous aimeriez ajouter à votre projet.

Prenez-le temps d'en discuter en binome et plus globalement avec la totalité du groupe afin de lister les fonctionnalités que vous aimeriez ajouter à votre jeu. Donner leur aussi une priorité.

Suite à ce brainstorming, vous ajouterez ou mettre à jour un fichier `TO_DO.md` (ou `TO_DO.md`) à votre projet dans lequel vous listerez, par ordre de priorité, ces différentes fonctionnalités.

Et si vous avez encore beaucoup de temps, vous pouvez commencer à réaliser certaines de ces fonctionnalités ! 


## Pour finir 

+ <span style='color:blue'>Faire un commit de vos derniers changements.</span> 
+ <span style='color:blue'>Tagger ce dernier commit </span> 
+ <span style='color:blue'>Pousser (Push) votre code vers votre dépôt distant sur GitLab.</span> 
+ <span style='color:blue'>Faire un test de couverture de code de votre MVP et pousser le bilan obtenu vers votre dépôt distant sur GitLab.</span>


